from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

# Create your views here.

def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos
    }
    return render(request, "todos/todo_list.html", context)

def todo_list_detail(request, id):
    display_task = TodoList.objects.get(id=id)
    context = {
        "display_task": display_task
    }
    return render(request, "todos/todo_detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/todo_create.html", context)

def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=post)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.id)
    else:
        form = TodoForm(instance=post)
    context = {
        "form": form,
        "todo_list_object": post,
    }
    return render(request, "todos/todo_edit.html", context)

def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_create.html", context)

def todo_item_update(request,id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "form": form,
        "todo_item_object": post,
    }
    return render(request, "todos/todo_item_update.html", context)
